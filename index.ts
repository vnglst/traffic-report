import * as puppeteer from 'puppeteer';
import Telegraf from 'telegraf';
import { config } from "dotenv"

// load dotenv vars
config();

// initialize bot
const bot = new Telegraf(process.env.BOT_TOKEN)

const getReport = async () => {
    const browser = await puppeteer.launch({
        args: [
            // Required for Docker version of Puppeteer
            '--no-sandbox',
            '--disable-setuid-sandbox',
            // This will write shared memory files into /tmp instead of /dev/shm,
            // because Docker’s default for /dev/shm is 64MB
            '--disable-dev-shm-usage'
        ]
    })
    const page = await browser.newPage()
    await page.setViewport({ width: 1280, height: 800 })
    await page.goto('https://www.google.com/maps/dir/Gildstraat+147,+3572+EM+Utrecht/Houthavens,+Amsterdam/@52.2610064,4.7613433,10z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x47c66f4b3e1287d1:0xb2b1c77fbdfe935e!2m2!1d5.1356648!2d52.0971274!1m5!1m1!1s0x47c6082bfc49a16b:0xffc28b19767b8bfa!2m2!1d4.8841521!2d52.3924744!3e0')
    await page
        .waitForSelector('#section-directions-trip-0')

    await page.screenshot({ path: 'traffic-report.png', fullPage: true })

    const durationText = await page.evaluate(
        () => document
            .querySelector('#section-directions-trip-0')
            .querySelector('.section-directions-trip-duration')
            .textContent
    ); // 45 min

    await browser.close()

    return durationText
};


const main = async () => {
    const durationText = await getReport();

    bot.telegram.sendMessage(process.env.CHAT_ID, `travel time ${durationText}`);
    bot.telegram.sendPhoto(process.env.CHAT_ID, { source: './traffic-report.png' });

    // Use code below to find your chat id
    // bot.command('chatid', (ctx) => {
    //     console.log(ctx.update.message.from);
    //     ctx.reply(`Our chatId is: ${ctx.update.message.from.id}`);
    // });
    //
    // bot.launch()
}

main();




